# Vanilla Inspired Water for OpenMW

 A more vanilla looking water shader for OpenMW.

#### Credits

**Authors**:

* DassiD
* Vtastek
* Left4No aka Ingram

**Updated for OpenMW 0.48.0 and 0.49.0 by**:

* Qlonever (shader edits, packaging)
* johnnyhostile (packaging)

**Special Thanks**:

* Poodlesandwich for letting me update the Nexus page
* Qlonever for sharing their edits and packaging
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/vanilla-inspired-water-for-openmw/)

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/49391)

[Source on GitLab](https://gitlab.com/modding-openmw/vanilla-inspired-water-for-openmw)

#### Installation

1. Download the zip from a link above.
1. Extract it to a location of your choosing e.g., `C:\games\OpenMWMods\Shaders\VanillaInspiredWaterforOpenMW`
1. Add the following to your `openmw.cfg` file:

        # If you're using OpenMW 0.48.0 (the latest release) do this:
        resources="C:\games\OpenMWMods\Shaders\VanillaInspiredWaterforOpenMW\0.48.0\resources"

        # If you're using OpenMW 0.49.0 (dev build) do this:
        resources="C:\games\OpenMWMods\Shaders\VanillaInspiredWaterforOpenMW\0.49.0\resources"

1. Enjoy!

Please note that the 0.49.0 dev build variants may go out of date quickly so use them at your own risk. If they are out of date please let us know at the Discord or GitLab links below!

The `Morrowind_Pixel_Water_Imitation_for_OpenMW.zip` file contains _only_ the edited files and not the entire resources contents from OpenMW itself, use this if you prefer instead of the pre-packaged resources.

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/vanilla-inspired-water-for-openmw/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/49391?tab=posts)

## Vanilla Inspired Water for OpenMW

#### 2.0

* Updated to support OpenMW 0.48.0 and 0.49.0 dev builds (as of 2023-07-28) - big thanks to Qlonever on Discord for edits and packaging!

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/49391)

#### 1.0

* Original release

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/49391)

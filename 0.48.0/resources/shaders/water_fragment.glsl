#version 120

#if @useUBO
    #extension GL_ARB_uniform_buffer_object : require
#endif

#if @useGPUShader4
    #extension GL_EXT_gpu_shader4: require
#endif

#include "openmw_fragment.h.glsl"

#define REFRACTION @refraction_enabled
#define RAIN_RIPPLE_DETAIL @rain_ripple_detail

// Inspired by Blender GLSL Water by martinsh ( https://devlog-martinsh.blogspot.de/2012/07/waterundewater-shader-wip.html )

// tweakables -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

const float BIG_WAVES_X = 0.1; // strength of big waves
const float BIG_WAVES_Y = 0.1;

const float MID_WAVES_X = 0.15; // strength of middle sized waves
const float MID_WAVES_Y = 0.15;
const float MID_WAVES_RAIN_X = 0.25;
const float MID_WAVES_RAIN_Y = 0.25;

const float SMALL_WAVES_X = 0.15; // strength of small waves
const float SMALL_WAVES_Y = 0.15;
const float SMALL_WAVES_RAIN_X = 0.25;
const float SMALL_WAVES_RAIN_Y = 0.25;

const float WAVE_CHOPPYNESS = 0.05;                  // wave choppyness
const float WAVE_SCALE = 50.0;                       // overall wave scale

const float BUMP = 3.75;                             // overall water surface bumpiness
const float BUMP_RAIN = 6.0;
const float REFL_BUMP = 0.10;                        // reflection distortion amount
const float REFR_BUMP = 0.10;                        // refraction distortion amount

const vec3 SUN_EXT = vec3(-0.45, -0.55, -0.68);      // sunlight extinction

const float SPEC_HARDNESS = 1024.0;                  // specular highlights hardness

const float BUMP_SUPPRESS_DEPTH = 300.0;             // at what water depth bumpmap will be suppressed for reflections and refractions (prevents artifacts at shores)

const vec2 WIND_DIR = vec2(0.5f, -0.8f);
const float WIND_SPEED = 0.12f;

const float ALPHA_REDUCE = 0.35;                     // default value from Morrowind.ini
const vec3 ENV_REDUCE_COLOR = vec3 (255, 255, 255);  // default value from Morrowind.ini
const vec3 LERP_CLOSE_COLOR = vec3 (37, 46, 48);     // default value from Morrowind.ini

// ---------------- rain ripples related stuff ---------------------

const float RAIN_RIPPLE_GAPS = 10.0;
const float RAIN_RIPPLE_RADIUS = 0.2;

float scramble(float x, float z)
{
    return fract(pow(fract(x)*3.0+1.0, z));
}

vec2 randOffset(vec2 c, float time)
{
  time = fract(time/1000.0);
  c = vec2(c.x * c.y /  8.0 + c.y * 0.3 + c.x * 0.2,
           c.x * c.y / 14.0 + c.y * 0.5 + c.x * 0.7);
  c.x *= scramble(scramble(time + c.x/1000.0, 4.0), 3.0) + 1.0;
  c.y *= scramble(scramble(time + c.y/1000.0, 3.5), 3.0) + 1.0;
  return fract(c);
}

float randPhase(vec2 c)
{
  return fract((c.x * c.y) /  (c.x + c.y + 0.1));
}

float blip(float x)
{
  x = max(0.0, 1.0-x*x);
  return x*x*x;
}

float blipDerivative(float x)
{
  x = clamp(x, -1.0, 1.0);
  float n = x*x-1.0;
  return -6.0*x*n*n;
}

const float RAIN_RING_TIME_OFFSET = 1.0/6.0;

vec4 circle(vec2 coords, vec2 corner, float adjusted_time)
{
  vec2 center = vec2(0.5,0.5) + (0.5 - RAIN_RIPPLE_RADIUS) * (2.0 * randOffset(corner, floor(adjusted_time)) - 1.0);
  float phase = fract(adjusted_time);
  vec2 toCenter = coords - center;

  float r = RAIN_RIPPLE_RADIUS;
  float d = length(toCenter);
  float ringfollower = (phase-d/r)/RAIN_RING_TIME_OFFSET-1.0; // -1.0 ~ +1.0 cover the breadth of the ripple's ring

#if RAIN_RIPPLE_DETAIL > 0
// normal mapped ripples
  if(ringfollower < -1.0 || ringfollower > 1.0)
    return vec4(0.0);

  if(d > 1.0) // normalize center direction vector, but not for near-center ripples
    toCenter /= d;

  float height = blip(ringfollower*2.0+0.5); // brighten up outer edge of ring; for fake specularity
  float range_limit = blip(min(0.0, ringfollower));
  float energy = 1.0-phase;

  vec2 normal2d = -toCenter*blipDerivative(ringfollower)*5.0;
  vec3 normal = vec3(normal2d, 0.5);
  vec4 ret = vec4(normal, height);
  ret.xyw *= energy*energy;
  // do energy adjustment here rather than later, so that we can use the w component for fake specularity
  ret.xyz = normalize(ret.xyz) * energy*range_limit;
  ret.z *= range_limit;
  return ret;
#else
// ring-only ripples
  if(ringfollower < -1.0 || ringfollower > 0.5)
    return vec4(0.0);

  float energy = 1.0-phase;
  float height = blip(ringfollower*2.0+0.5)*energy*energy; // fake specularity

  return vec4(0.0, 0.0, 0.0, height);
#endif
}
vec4 rain(vec2 uv, float time)
{
  uv *= RAIN_RIPPLE_GAPS;
  vec2 f_part = fract(uv);
  vec2 i_part = floor(uv);
  float adjusted_time = time * 1.2 + randPhase(i_part);
#if RAIN_RIPPLE_DETAIL > 0
  vec4 a = circle(f_part, i_part, adjusted_time);
  vec4 b = circle(f_part, i_part, adjusted_time - RAIN_RING_TIME_OFFSET);
  vec4 c = circle(f_part, i_part, adjusted_time - RAIN_RING_TIME_OFFSET*2.0);
  vec4 d = circle(f_part, i_part, adjusted_time - RAIN_RING_TIME_OFFSET*3.0);
  vec4 ret;
  ret.xy = a.xy - b.xy/2.0 + c.xy/4.0 - d.xy/8.0;
  // z should always point up
  ret.z  = a.z  + b.z /2.0 + c.z /4.0 + d.z /8.0;
  //ret.xyz *= 1.5;
  // fake specularity looks weird if we use every single ring, also if the inner rings are too bright 
  ret.w  = (a.w + c.w /8.0)*1.5;
  return ret;
#else
  return circle(f_part, i_part, adjusted_time) * 1.5;
#endif
}

vec2 complex_mult(vec2 a, vec2 b)
{
    return vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}
vec4 rainCombined(vec2 uv, float time) // returns ripple normal in xyz and fake specularity in w
{
  return
    rain(uv, time)
  + rain(complex_mult(uv, vec2(0.4, 0.7)) + vec2(1.2, 3.0),time)
    #if RAIN_RIPPLE_DETAIL == 2
      + rain(uv * 0.75 + vec2( 3.7,18.9),time)
      + rain(uv * 0.9  + vec2( 5.7,30.1),time)
      + rain(uv * 1.0  + vec2(10.5 ,5.7),time)
    #endif
  ;
}


// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

vec2 normalCoords(vec2 uv, float scale, float speed, float time, float timer1, float timer2, vec3 previousNormal)
  {
    return uv * (WAVE_SCALE * scale) + WIND_DIR * time * (WIND_SPEED * speed) -(previousNormal.xy/previousNormal.zz) * WAVE_CHOPPYNESS + vec2(time * timer1,time * timer2);
  }

varying vec4 position;
varying float linearDepth;

uniform sampler2D normalMap;

uniform float osg_SimulationTime;

uniform float near;
uniform vec3 nodePosition;

uniform float rainIntensity;

uniform vec2 screenRes;

#define PER_PIXEL_LIGHTING 0

#include "shadows_fragment.glsl"
#include "lighting.glsl"
#include "fog.glsl"

float frustumDepth;

float linearizeDepth(float depth)
{
#if @reverseZ
  depth = 1.0 - depth;
#endif
  float z_n = 2.0 * depth - 1.0;
  depth = 2.0 * near * far / (far + near - z_n * frustumDepth);
  return depth;
}

void main(void)
{
    frustumDepth = abs(far - near);
    vec3 worldPos = position.xyz + nodePosition.xyz;
    vec2 UV = worldPos.xy / (8192.0*5.0) * 3.0;
    UV.y *= -1.0;

    float shadow = unshadowedLightRatio(linearDepth);

    vec2 screenCoords = gl_FragCoord.xy / screenRes;
    
    vec3 normalCorrection = vec3 (1.008, 1.006, 1.0);

    #define waterTimer osg_SimulationTime

    vec3 normal0 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.0125, 0.01, waterTimer, -0.00375, -0.00125, vec3(0.0,0.0,0.0))).rgb - normalCorrection;
    vec3 normal1 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.025,  0.02, waterTimer,  0.005,    0.00375, normal0)).rgb - normalCorrection;
    vec3 normal2 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.25,   0.07, waterTimer, -0.04,    -0.03,    normal1)).rgb - normalCorrection;
    vec3 normal3 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.5,    0.09, waterTimer,  0.03,     0.04,    normal2)).rgb - normalCorrection;
    vec3 normal4 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.25,   0.07, waterTimer, -0.01,     0.05,    normal3)).rgb - normalCorrection;
    vec3 normal5 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.5,    0.09, waterTimer,  0.05,    -0.03,    normal4)).rgb - normalCorrection;

    vec4 rainRipple;

    if (rainIntensity > 0.01)
      rainRipple = rainCombined(position.xy/1000.0, waterTimer) * clamp(rainIntensity, 0.0, 1.0);
    else
      rainRipple = vec4(0.0);

    vec3 rippleAdd = rainRipple.xyz * 20.0;

    vec2 bigWaves = vec2(BIG_WAVES_X,BIG_WAVES_Y);
    vec2 midWaves = mix(vec2(MID_WAVES_X,MID_WAVES_Y),vec2(MID_WAVES_RAIN_X,MID_WAVES_RAIN_Y),rainIntensity);
    vec2 smallWaves = mix(vec2(SMALL_WAVES_X,SMALL_WAVES_Y),vec2(SMALL_WAVES_RAIN_X,SMALL_WAVES_RAIN_Y),rainIntensity);
    float bump = mix(BUMP,BUMP_RAIN,rainIntensity);

    vec3 normal = (normal0 * bigWaves.x + normal1 * bigWaves.y + normal2 * midWaves.x +
                   normal3 * midWaves.y + normal4 * smallWaves.x + normal5 * smallWaves.y);
    normal = normalize(vec3(-normal.x * bump, -normal.y * bump, normal.z) + vec3(-rippleAdd.x, -rippleAdd.y, rippleAdd.z));

    vec3 lVec = normalize((gl_ModelViewMatrixInverse * vec4(lcalcPosition(0).xyz, 0.0)).xyz);
    vec3 cameraPos = (gl_ModelViewMatrixInverse * vec4(0,0,0,1)).xyz;
    vec3 vVec = normalize(position.xyz - cameraPos.xyz);

    float sunFade = length(gl_LightModel.ambient.xyz);

    // fake fresnel
    float fresnel = clamp(ALPHA_REDUCE + 0.85 * (1.0 - abs(dot(vVec, normal))), 0.0, 1.0);

    float radialise = 1.0;

#if @radialFog
    float radialDepth = distance(position.xyz, cameraPos);
    // TODO: Figure out how to properly radialise refraction depth and thus underwater fog
    // while avoiding oddities when the water plane is close to the clipping plane
    // radialise = radialDepth / linearDepth;
#else
    float radialDepth = 0.0;
#endif

    // specular
    float specular = pow(clamp(1.0006 * dot(reflect(vVec, normalize(normal * vec3 (REFL_BUMP, REFL_BUMP, 1.0))), lVec), 0.0, 1.0),SPEC_HARDNESS) * shadow;

    vec3 waterColor = LERP_CLOSE_COLOR / 255.0;
    // uncomment this line to make the water color darken at night; Morrowind doesn't do this, causing the water to glow in dark environments
    //waterColor *= (0.5 * sunFade + 0.5);

    vec4 sunSpec = lcalcSpecular(0);
    
    vec2 screenCoordsOffset = normal.xy;

#if REFRACTION
    float depthSample = linearizeDepth(mw_sampleRefractionDepthMap(screenCoords)) * radialise;
    float surfaceDepth = linearizeDepth(gl_FragCoord.z) * radialise;
    float realWaterDepth = depthSample - surfaceDepth;  // undistorted water depth in view direction, independent of frustum
    screenCoordsOffset *= clamp(realWaterDepth / BUMP_SUPPRESS_DEPTH,0.05,1);

    // refraction
    vec3 refraction = mw_sampleRefractionMap(screenCoords - screenCoordsOffset * REFR_BUMP).rgb;

    // brighten up the refraction underwater
    if (cameraPos.z < 0.0)
        refraction = clamp(refraction * 1.5, 0.0, 1.0);
#endif

// reflection
    vec3 reflection = mw_sampleReflectionMap(screenCoords + screenCoordsOffset * REFL_BUMP).rgb * ENV_REDUCE_COLOR / 255.0 + min(specular * sunSpec.w * 2.0, 1.0) * exp(lVec.z * SUN_EXT);
    reflection = mix(waterColor, reflection, 1.0 - abs(vVec.z));
    
#if REFRACTION
    gl_FragData[0].xyz = mix(refraction, reflection,  fresnel);
    gl_FragData[0].w = 1.0;
#else
    gl_FragData[0].xyz = reflection.xyz;
    gl_FragData[0].w = fresnel;
#endif

    gl_FragData[0] = applyFogAtDist(gl_FragData[0], radialDepth, linearDepth);

#if !@disableNormals
    gl_FragData[1].rgb = normal * 0.5 + 0.5;
#endif

    applyShadowDebugOverlay();
}

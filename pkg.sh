#!/bin/sh
set -eu

file_name=vanilla-inspired-water-for-openmw.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths \
    $file_name \
    Morrowind_Pixel_Water_Imitation_for_OpenMW.zip \
    "0.48.0" \
    "0.49.0" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt

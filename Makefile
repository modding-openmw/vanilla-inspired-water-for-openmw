pkg:
	./pkg.sh

clean:
	rm -f vanilla-inspired-water-for-openmw.zip *.sha*sum.txt version.txt

web-clean:
	cd web && rm -rf build site/*.md sha256sums*

web: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

clean-all: clean web-clean

local-web:
	cd web && python3 -m http.server -d build

#version 120

#if @useUBO
    #extension GL_ARB_uniform_buffer_object : require
#endif

#if @useGPUShader4
    #extension GL_EXT_gpu_shader4: require
#endif

#include "lib/core/fragment.h.glsl"

#define REFRACTION @refraction_enabled

// Inspired by Blender GLSL Water by martinsh ( https://devlog-martinsh.blogspot.de/2012/07/waterundewater-shader-wip.html )

// tweakables -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

const float BIG_WAVES_X = 0.1; // strength of big waves
const float BIG_WAVES_Y = 0.1;

const float MID_WAVES_X = 0.15; // strength of middle sized waves
const float MID_WAVES_Y = 0.15;
const float MID_WAVES_RAIN_X = 0.25;
const float MID_WAVES_RAIN_Y = 0.25;

const float SMALL_WAVES_X = 0.15; // strength of small waves
const float SMALL_WAVES_Y = 0.15;
const float SMALL_WAVES_RAIN_X = 0.25;
const float SMALL_WAVES_RAIN_Y = 0.25;

const float WAVE_CHOPPYNESS = 0.05;                  // wave choppyness
const float WAVE_SCALE = 50.0;                       // overall wave scale

const float BUMP = 3.75;                             // overall water surface bumpiness
const float BUMP_RAIN = 6.0;
const float REFL_BUMP = 0.10;                        // reflection distortion amount
const float REFR_BUMP = 0.10;                        // refraction distortion amount

const vec3 SUN_EXT = vec3(-0.45, -0.55, -0.68);      // sunlight extinction

const float SPEC_HARDNESS = 1024.0;                  // specular highlights hardness

const float BUMP_SUPPRESS_DEPTH = 300.0;             // at what water depth bumpmap will be suppressed for reflections and refractions (prevents artifacts at shores)

const vec2 WIND_DIR = vec2(0.5f, -0.8f);
const float WIND_SPEED = 0.12f;

const float ALPHA_REDUCE = 0.35;                     // default value from Morrowind.ini
const vec3 ENV_REDUCE_COLOR = vec3 (255, 255, 255);  // default value from Morrowind.ini
const vec3 LERP_CLOSE_COLOR = vec3 (37, 46, 48);     // default value from Morrowind.ini

// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

vec2 normalCoords(vec2 uv, float scale, float speed, float time, float timer1, float timer2, vec3 previousNormal)
{
  return uv * (WAVE_SCALE * scale) + WIND_DIR * time * (WIND_SPEED * speed) -(previousNormal.xy/previousNormal.zz) * WAVE_CHOPPYNESS + vec2(time * timer1,time * timer2);
}

uniform sampler2D rippleMap;
uniform sampler2D refractionMap;
uniform vec3 playerPos;

varying vec3 worldPos;

varying vec2 rippleMapUV;

varying vec4 position;
varying float linearDepth;

uniform sampler2D normalMap;

uniform float osg_SimulationTime;

uniform float near;
uniform float far;

uniform float rainIntensity;

uniform vec2 screenRes;

#define PER_PIXEL_LIGHTING 0

#include "shadows_fragment.glsl"
#include "lib/light/lighting.glsl"
#include "fog.glsl"
#include "lib/water/fresnel.glsl"
#include "lib/water/rain_ripples.glsl"
#include "lib/view/depth.glsl"

void main(void)
{
    vec2 UV = worldPos.xy / (8192.0*5.0) * 3.0;
    UV.y *= -1.0;

    float shadow = unshadowedLightRatio(linearDepth);

    vec2 screenCoords = gl_FragCoord.xy / screenRes;
    
    vec3 normalCorrection = vec3 (1.008, 1.006, 1.0);

    #define waterTimer osg_SimulationTime

    vec3 normal0 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.0125, 0.01, waterTimer, -0.00375, -0.00125, vec3(0.0,0.0,0.0))).rgb - normalCorrection;
    vec3 normal1 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.025,  0.02, waterTimer,  0.005,    0.00375, normal0)).rgb - normalCorrection;
    vec3 normal2 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.25,   0.07, waterTimer, -0.04,    -0.03,    normal1)).rgb - normalCorrection;
    vec3 normal3 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.5,    0.09, waterTimer,  0.03,     0.04,    normal2)).rgb - normalCorrection;
    vec3 normal4 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.25,   0.07, waterTimer, -0.01,     0.05,    normal3)).rgb - normalCorrection;
    vec3 normal5 = 2.0 * texture2D(normalMap,normalCoords(UV, 0.5,    0.09, waterTimer,  0.05,    -0.03,    normal4)).rgb - normalCorrection;

    vec4 rainRipple;

    if (rainIntensity > 0.01)
        rainRipple = rainCombined(position.xy/1000.0, waterTimer) * clamp(rainIntensity, 0.0, 1.0);
    else
        rainRipple = vec4(0.0);

    vec3 rippleAdd = rainRipple.xyz * 20.0;

    float distToCenter = length(rippleMapUV - vec2(0.5));
    float blendClose = smoothstep(0.001, 0.02, distToCenter);
    float blendFar = 1.0 - smoothstep(0.3, 0.4, distToCenter);
    float distortionLevel = 1.5;
    rippleAdd += distortionLevel * vec3(texture2D(rippleMap, rippleMapUV).ba * blendFar * blendClose, 0.0);

    vec2 bigWaves = vec2(BIG_WAVES_X,BIG_WAVES_Y);
    vec2 midWaves = mix(vec2(MID_WAVES_X,MID_WAVES_Y),vec2(MID_WAVES_RAIN_X,MID_WAVES_RAIN_Y),rainIntensity);
    vec2 smallWaves = mix(vec2(SMALL_WAVES_X,SMALL_WAVES_Y),vec2(SMALL_WAVES_RAIN_X,SMALL_WAVES_RAIN_Y),rainIntensity);
    float bump = mix(BUMP,BUMP_RAIN,rainIntensity);
    
    vec3 normal = (normal0 * bigWaves.x + normal1 * bigWaves.y + normal2 * midWaves.x +
                   normal3 * midWaves.y + normal4 * smallWaves.x + normal5 * smallWaves.y);
    normal = normalize(vec3(-normal.x * bump, -normal.y * bump, normal.z) + vec3(-rippleAdd.x, -rippleAdd.y, rippleAdd.z));

    vec3 lVec = normalize((gl_ModelViewMatrixInverse * vec4(lcalcPosition(0).xyz, 0.0)).xyz);
    vec3 cameraPos = (gl_ModelViewMatrixInverse * vec4(0,0,0,1)).xyz;
    vec3 vVec = normalize(position.xyz - cameraPos.xyz);

    float sunFade = length(gl_LightModel.ambient.xyz);

    // fake fresnel
    float fresnel = clamp(ALPHA_REDUCE + 0.85 * (1.0 - abs(dot(vVec, normal))), 0.0, 1.0);
    
    float radialise = 1.0;

#if @radialFog
    float radialDepth = distance(position.xyz, cameraPos);
    // TODO: Figure out how to properly radialise refraction depth and thus underwater fog
    // while avoiding oddities when the water plane is close to the clipping plane
    // radialise = radialDepth / linearDepth;
#else
    float radialDepth = 0.0;
#endif

    // specular
    float specular = pow(clamp(1.0006 * dot(reflect(vVec, normalize(normal * vec3 (REFL_BUMP, REFL_BUMP, 1.0))), lVec), 0.0, 1.0),SPEC_HARDNESS) * shadow;

    vec3 waterColor = LERP_CLOSE_COLOR / 255.0;
    // uncomment this line to make the water color darken at night; Morrowind doesn't do this, causing the water to glow in dark environments
    //waterColor *= (0.5 * sunFade + 0.5);

    vec4 sunSpec = lcalcSpecular(0);

    vec2 screenCoordsOffset = normal.xy;

#if REFRACTION
    float depthSample = linearizeDepth(sampleRefractionDepthMap(screenCoords), near, far) * radialise;
    float surfaceDepth = linearizeDepth(gl_FragCoord.z, near, far) * radialise;
    float realWaterDepth = max(depthSample - surfaceDepth, 0);  // undistorted water depth in view direction, independent of frustum
    screenCoordsOffset *= clamp(realWaterDepth / BUMP_SUPPRESS_DEPTH,0.05,1);

    // refraction
    vec3 refraction = sampleRefractionMap(screenCoords - screenCoordsOffset * REFR_BUMP).rgb;

    // brighten up the refraction underwater
    if (cameraPos.z < 0.0)
        refraction = clamp(refraction * 1.5, 0.0, 1.0);
#endif

    // reflection
    vec3 reflection = sampleReflectionMap(screenCoords + screenCoordsOffset * REFL_BUMP).rgb * ENV_REDUCE_COLOR / 255.0 + min(specular * sunSpec.w * 2.0, 1.0) * exp(lVec.z * SUN_EXT);
    reflection = mix(waterColor, reflection, 1.0 - abs(vVec.z));
    
#if REFRACTION
    gl_FragData[0].xyz = mix(refraction, reflection.xyz, fresnel);
    gl_FragData[0].w = 1.0;
#else
    gl_FragData[0].xyz = reflection.xyz;
    gl_FragData[0].w = fresnel;
#endif

    gl_FragData[0] = applyFogAtDist(gl_FragData[0], radialDepth, linearDepth, far);

#if !@disableNormals
    gl_FragData[1].rgb = normalize(gl_NormalMatrix * normal) * 0.5 + 0.5;
#endif

    applyShadowDebugOverlay();
}
